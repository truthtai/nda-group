const request = require('superagent');

export default function settingSocial() {
  return new Promise((resolve, reject) => {
    request
      .get(`http://api.ndagroup.com.vn/setting/SOCIAL`)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res.body);
      });
  });
}
