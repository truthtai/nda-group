const request = require('superagent');

export default function view(req) {
  return new Promise((resolve, reject) => {
    const api = `http://api.ndagroup.com.vn/posts/view/${req.query.id}`;
    request
      .get(api)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res.body);
      });
  });
}
