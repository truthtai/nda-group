export default function lang() {
  const langUse = localStorage.getItem('lang') || 'en_EU';
  return Promise.resolve(langUse);
}
