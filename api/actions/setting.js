const request = require('superagent');

export default function setting(req) {
  return new Promise((resolve, reject) => {
    request
      .get(`http://api.ndagroup.com.vn/setting/LANG/${req.query.id}`)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res.body);
      });
  });
}
