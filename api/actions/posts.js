const request = require('superagent');

export default function posts(req) {
  return new Promise((resolve, reject) => {
    const api = `http://api.ndagroup.com.vn/category/${req.query.id}/posts`;
    console.log(api);
    request
      .get(api)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          reject(err);
        }
        console.log(res.body);
        resolve(res.body);
      });
  });
}
