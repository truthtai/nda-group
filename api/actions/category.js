const request = require('superagent');

export default function category(req) {
  return new Promise((resolve, reject) => {
    const api = `http://api.ndagroup.com.vn/category/name/${req.query.name}`;
    request
      .get(api)
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res.body);
      });
  });
}
