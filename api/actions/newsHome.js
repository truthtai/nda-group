const request = require('superagent');

export default function newsHome() {
  return new Promise((resolve, reject) => {
    request
      .get('http://api.ndagroup.com.vn/news')
      .set('Accept', 'application/json')
      .end((err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res.body);
      });
  });
}
