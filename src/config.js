require('babel/polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT,
  app: {
    title: 'NDA GROUP',
    description: 'NDA GROUP',
    head: {
      titleTemplate: 'NDA GROUP: %s',
      meta: [
        {name: 'description', content: 'NDA GROUP.'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'NDA GROUP'},
        {property: 'og:image', content: 'logo.png'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'NDA GROUP'},
        {property: 'og:description', content: 'NDA GROUP'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: '@truthtaicom'},
        {property: 'og:creator', content: '@truthtaicom'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ],
      link: [
        {rel: 'https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic&subset=latin,vietnamese'},
        {rel: 'https://fonts.googleapis.com/css?family=Oswald:400,300,700'},
        // {rel: 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css'},
        { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css' }
      ],
      script: [
        {src: 'http://code.jquery.com/jquery-2.2.2.min.js'},
        {src: 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox-plus-jquery.min.js'}
      ]
    }
  },

}, environment);
