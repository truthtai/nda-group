import React from 'react';
import { IndexRoute, Route } from 'react-router';
import {
    App,
    Home,
    NotFound,
    KnowUs,
    KnowusVisionValue,
    KnowusTeam,
    KnowusCareers,
    Purpose,
    Provide,
    CreativeVision,
    PublicationsAwards,
    News,
    ContactUs,
    Intro,
    Post
  } from 'containers';

export default () => {
  // store.dispatch(getlang());

  // console.log(store);
  // const lang = window.localStorage.getItem('lang') || 'en_EU';

  // const requireLogin = (nextState, replaceState, cb) => {
  //   function checkAuth() {
  //     const { auth: { user }} = store.getState();
  //     if (!user) {
  //       // oops, not logged in, so can't be here!
  //       replaceState(null, '/');
  //     }
  //     cb();
  //   }

  //   if (!isAuthLoaded(store.getState())) {
  //     store.dispatch(loadAuth()).then(checkAuth);
  //   } else {
  //     checkAuth();
  //   }
  // };

  /**
   * Please keep routes in alphabetical order
   */
  return (
    <Route path="/" component={App}>
      { /* Home (main) route */ }
      <IndexRoute component={Intro} />

      { /* Routes requiring login */ }
      { /*
      <Route onEnter={requireLogin}>
        <Route path="chat" component={Chat}/>
        <Route path="loginSuccess" component={LoginSuccess}/>
      </Route>

    */ }

      { /* Routes */ }
      <Route path="knowus/vision-values" component={KnowusVisionValue} />
      <Route path="knowus/team" component={KnowusTeam} />
      <Route path="knowus/news" component={News} />
      <Route path="knowus/careers" component={KnowusCareers} />

      <Route path="knowus" component={KnowUs} />
      <Route path="purpose/:p1/:path" component={Purpose} />
      <Route path="purpose/:p1" component={Purpose} />

      <Route path="provide/:p1/:path" component={Provide} />
      <Route path="provide/:p1" component={Provide} />

      <Route path="publications-awards/:p1" component={PublicationsAwards} />
      <Route path="publications-awards" component={PublicationsAwards} />

      <Route path="creative-vision" component={CreativeVision} />
      <Route path="our-news" component={News} />

      <Route path="contact-us" component={ContactUs} />
      <Route path="home" component={Home} />
      <Route path="view/:id" component={Post} />
      { /* Catch all route */ }

      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
