import React, {Component, PropTypes} from 'react';


const styles = require('./FileItem.scss');
export default class FileItem extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  };

  render() {
    const {data} = this.props;
    return (
      <div className={styles.root}>
        <div className={'container'}>
          <h3 className={'titleSector'}> Download </h3>

          <div className={'row'}>
            {
              data.map((dt, index) => {
                return (
                  <div className={'col-md-6'} key={index}>
                    <h5>
                      <a href="#"><i className="fa fa-file-text-o"></i> {dt.title}</a>
                    </h5>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
