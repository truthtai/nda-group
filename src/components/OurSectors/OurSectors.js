import React, {Component} from 'react';
import __ from 'lodash';

const styles = require('./OurSectors.scss');
export default class OurSectors extends Component {

  render() {
    const oursectors = [
      {
        id: 1,
        title: 'Waterfront & Marinas',
        img: '/thumbHome/waterfront-marinas.jpg',
        color: '102,153,204',
        sector: 1,
        col: 4,
        url: 'WATERFRONT_MARINAS'
      }, {
        id: 2,
        title: 'Mountain',
        img: '/thumbHome/mountain.jpg',
        color: '153,255,255',
        sector: 1,
        col: 4,
        url: 'MOUNTAIN'
      }, {
        id: 3,
        title: 'Mixed Use',
        img: '/thumbHome/mixed-use.jpg',
        color: '102,153,102',
        sector: 1,
        col: 4,
        url: 'MIXES_USING'
      }, {
        id: 4,
        title: 'Residential',
        img: '/thumbHome/residential.jpg',
        color: '204,102,102',
        sector: 1,
        col: 4,
        url: 'RESIDEENTIAL'
      }, {
        id: 5,
        title: 'Tourism',
        img: '/thumbHome/tourism.jpg',
        color: '102,204,102',
        sector: 1,
        col: 8,
        url: 'TOURISM'
      }, {
        id: 6,
        title: 'Public & Culture',
        img: '/thumbHome/public-culture.jpg',
        color: '204,153,204',
        sector: 2,
        col: 6,
        url: 'PUBLIC_CULTURE'
      }, {
        id: 7,
        title: 'Professional Activities',
        img: '/thumbHome/professional-activities.jpg',
        color: '204,204,102',
        sector: 2,
        col: 6,
        url: 'PROFESINAL_ACTIVITIES'
      }, {
        id: 8,
        title: 'Tall Building',
        img: '/thumbHome/tall-building.jpg',
        color: '153,153,153',
        sector: 11,
        col: 12,
        url: 'TALL_BUILDING'
      }
    ];

    return (
      <div className={styles.root}>
        <div className={'row'}>
          <div className={'col-md-9'}>
            <div className={'row'}>
              {
                __.map(__.filter(oursectors, (it) => {
                  return it.sector === 1;
                }), (item, index) => {
                  return (
                    <div className={'col-md-' + item.col} key={index}>
                      <div className={styles.sectorsItem}>
                        <a href={'/purpose/' + item.url}>
                          <img src={item.img} />
                          <span><h3 style={{'borderBottom': `5px solid rgb(${item.color})`}}>{item.title}</h3></span>
                        </a>
                      </div>
                    </div>
                  );
                })
              }
            </div>
          </div>
          <div className={'col-md-3'}>
            <div className={'row'}>
             {
                __.map(__.filter(oursectors, (it) => {
                  return it.sector === 11;
                }), (item, index) => {
                  return (
                    <div className={'col-md-' + item.col} key={index}>
                      <div className={styles.sectorsItem}>
                        <a href={'/purpose/' + item.url}>
                          <img src={item.img} style={{'height': '530px'}}/>
                           <span><h3 style={{'borderBottom': `5px solid rgb(${item.color})`}}>{item.title}</h3></span>
                        </a>
                      </div>
                    </div>
                  );
                })
              }
            </div>
          </div>
        </div>

        <div className={'row'}>
            {
              __.map(__.filter(oursectors, (it) => {
                return it.sector === 2;
              }), (item, index) => {
                return (
                  <div className={'col-md-' + item.col} key={index}>
                    <div className={styles.sectorsItem}>
                      <a href={'/purpose/' + item.url}>
                        <img src={item.img} />
                        <span><h3 style={{'borderBottom': `5px solid rgb(${item.color})`}}>{item.title}</h3></span>
                      </a>
                    </div>
                  </div>
                );
              })
            }
        </div>
      </div>
    );
  }
}
