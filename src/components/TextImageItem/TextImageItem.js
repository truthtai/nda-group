import React, {Component, PropTypes} from 'react';


const styles = require('./TextImageItem.scss');
export default class TextImageItem extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    routes: PropTypes.array,
    params: PropTypes.object
  }

  render() {
    const {data, params} = this.props;
    return (
      <div className={styles.root}>
        <div className={'row'}>
          {
            data.map((item, index) => {
              const col = item.col ? item.col : '6';
              let url;
              if (params) {
                if (params.p1 && params.path) {
                  url = '/' + item.type + '/' + params.p1 + '/' + item.url;
                } else if (params.p1 && !params.path) {
                  url = '/' + item.type + '/' + params.p1 + '/' + item.url;
                } else {
                  url = '/' + item.type + '/' + item.url;
                }
              } else {
                url = '/' + item.type + '/' + item.url;
              }
              const isOnlyText = item.onlyText ? '1px solid #333' : '1px solid #fff';

              return (
                  <div className={'col-md-' + col} key={index}>
                    <div className={styles.servicesItem}>
                      <a href={url} style={{'border': isOnlyText}}>
                        { item.onlyText ? '' : <img src={item.img} /> }
                        <h3>{item.title}</h3>
                        <p>{item.description}</p>
                      </a>
                    </div>
                  </div>
                );
            })
          }
        </div>
      </div>
    );
  }
}
/*
0 - 1
2 - 3
4 - 5
6 - 7
0 - 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - 10
A - B - B - A - A - B - B - A - A
*/
// palindrom[i] = palindrom.charAt(palindrom.length)-1
