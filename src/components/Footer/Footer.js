import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { getSettingSocials } from '../../redux/modules/setting';
const styles = require('./Footer.scss');
const logoImage = require('../../../static/logo.png');

@connect(
  state => ({socials: state.setting.socials}),
  {getSettingSocials})

export default class Footer extends Component {
  static propTypes = {
    getSettingSocials: PropTypes.func,
    socials: PropTypes.array
  }

  componentDidMount() {
    this.props.getSettingSocials();
  }

  render() {
   // const {info, load} = this.props; // eslint-disable-line no-shadow
    const {socials} = this.props;
    return (
      <div className={styles.footerBar}>
        <div className={'container'}>
          <div className={'row'}>
            <div className={'col-md-2'}>
              <a href="/"><img className={styles.logo} src={logoImage} /></a>
            </div>
            <div className={'col-md-3'}>
              <ul className={'list-inline ' + styles.menu}>
                <li><a href="">Site Map</a></li>
                <li><a href="">Legal Notices</a></li>
              </ul>
            </div>
            <div className={'col-md-7'}>
              <ul className={'list-inline ' + styles.menu}>
                { socials.length &&
                  socials.map((social, index) => {
                    return (
                      <li key={index}><a href={social.URL}><img src={`/${social.name}.png`} /></a></li>
                    )
                  })
                }
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
