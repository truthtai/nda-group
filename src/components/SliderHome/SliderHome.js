import React, {Component, PropTypes} from 'react';
// import {connect} from 'react-redux';
import {Carousel, CarouselItem, Glyphicon} from 'react-bootstrap';
// import '../../theme/datepicker.scss';
import * as _ from 'lodash';

const styles = require('./SliderHome.scss');

export default class SliderHome extends Component {
  static propTypes = {
    data: PropTypes.array
  };

  render() {
    // const innerGlyphicon = <Glyphicon glyph="music" />;
    let { data } = this.props;
    if (!data) {
      data = [
        {
          file: 'http://ndagroup.com.vn/bg/bg.jpg',
          title: 'HEFEI TV CENTER, CHINA',
          description: 'Hefei Feicui TV Tower is a tower under construction in Hefei, China'
        }
      ]
    }

    const prevIcon = <Glyphicon glyph="menu-left" />;
    const nextIcon = <Glyphicon glyph="menu-right" />;

    return (
      <div className={styles.sliderHome}>
        <div className={styles.slider}>
          { data &&
            <Carousel nextIcon={nextIcon} prevIcon={prevIcon}>
            {
              data.map((ds, index) => {
                let baseURL;
                if (_.includes(ds.file, 'http')) {
                  baseURL = ds.file
                } else {
                  baseURL = `http://api.ndagroup.com.vn/uploads/${ds.file}`;
                }
                return (
                  <CarouselItem key={index}>
                    <img className={styles.slideImg} src={baseURL} />
                    { ds.title && ds.description &&
                      <div className="carousel-caption">
                        <h3>{ds.title}</h3>
                        <p>{ds.description}</p>
                      </div>
                    }

                  </CarouselItem>
                )
              })
            }
            </Carousel>
          }
        </div>

      </div>
    );
  }
}
