import React, {Component, PropTypes} from 'react';


const styles = require('./DiscoverItem.scss');
export default class DiscoverItem extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    params: PropTypes.object
  };

  render() {
    const {data, params} = this.props;
    return (
      <div className={styles.root}>
          <div className={'row'}>
          {
            data.map((item, index) => {
              let url;
              if (params) {
                if (params.p1 && params.path) {
                  url = '/' + item.type + '/' + item.url;
                } else if (params.p1 && !params.path) {
                  url = '/' + item.type + '/' + params.p1 + '/' + item.url;
                } else if (item.type) {
                  url = '/' + item.type + '/' + item.url;
                } else {
                  url = '/' + item.url;
                }
              } else {
                url = '/' + item.url;
              }
              return (
                  <div className={'col-md-6'} key={index}>
                    <div className={styles.item}>
                      <a href={url}>
                        <img src={item.img} />
                        <span className={styles.title}>
                          <span>{item.title}</span>
                          <h6>Discover</h6>
                        </span>
                      </a>
                    </div>
                  </div>
                );
            })
          }
          </div>
      </div>
    );
  }
}
