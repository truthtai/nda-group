import React, {Component, PropTypes} from 'react';


const styles = require('./TextItem.scss');
export default class TextItem extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  }

  render() {
    const {data} = this.props;
    return (
      <div className={styles.root}>
        <div className={'row'}>
          {
            data.map((item, index) => {
              const col = item.col ? item.col : '6';
              return (
                  <div className={'col-md-' + col} key={index}>
                    <div className={styles.textItem}>
                      <a href="#">
                        <h3>{item.title}</h3>
                        <p>{item.description}</p>
                      </a>
                    </div>
                  </div>
                );
            })
          }
        </div>
      </div>
    );
  }
}
