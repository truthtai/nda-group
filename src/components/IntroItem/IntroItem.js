import React, {Component, PropTypes} from 'react';


const styles = require('./IntroItem.scss');
export default class IntroItem extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired
  };

  render() {
    const {data} = this.props;
    return (
      <div className={styles.root}>
        <div className={'row'}>
          <div className={'col-md-12 vertical-align'}>
            <div className={'col-md-4'}>
              <img className={'img-responsive'} src={data.img} />
            </div>
            <div className={'col-md-8'}>
              <h5>{data.title}</h5>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
