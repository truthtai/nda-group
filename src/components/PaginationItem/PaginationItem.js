import React, {Component, PropTypes} from 'react';
import {Pagination} from 'react-bootstrap';

const styles = require('./PaginationItem.scss');
export default class PaginationItem extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  handleSelect = (event, selectedEvent) => {
    this.setState({
      activePage: selectedEvent.eventKey
    });
  }

  render() {
    const {data} = this.props;
    return (
      <div className={styles.root}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          items={data.items}
          maxButtons={data.maxBtn}
          activePage={data.current}
          onSelect={this.handleSelect} />
      </div>
    );
  }
}
