import React, {Component, PropTypes} from 'react';


const styles = require('./ServicesItem.scss');
export default class ServicesItem extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    params: PropTypes.object
  }

  render() {
    const {data, params} = this.props;
    return (
      <div className={styles.root}>
        <div className={'row'}>
          {
            data.map((item, index) => {
              const col = item.col ? item.col : '4';
              let url;
              if (params) {
                if (params.p1 && params.path) {
                  url = '/' + item.type + '/' + item.url;
                } else if (params.p1 && !params.path) {
                  url = '/' + item.type + '/' + params.p1 + '/' + item.url;
                } else if (item.type) {
                  url = '/' + item.type + '/' + item.url;
                } else {
                  url = '/' + item.url;
                }
              } else {
                url = '/' + item.url;
              }
              return (
                  <div className={'col-md-' + col} key={index}>
                    <div className={styles.servicesItem}>
                      <a href={url}>
                        <img src={item.img} />
                        <span className={styles.title}>
                          <span>{item.title}</span>
                        </span>
                      </a>
                    </div>
                  </div>
                );
            })
          }
        </div>
      </div>
    );
  }
}
