import React, {Component, PropTypes} from 'react';


const styles = require('./NewsItem.scss');
export default class NewsItem extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  };

  componentDidMount() {
  }

  render() {
    const {data} = this.props;
    // console.log(data.description.getElementsByTagName('img'));
    return (
      <div className={styles.root}>
        <div className={'row'}>
          { data.length > 0 &&
            data.map((item, index) => {
              const col = item.col ? item.col : '3';
              let image;
              let URL;
              if (!item.image) {
                image = $(item.description).find('img')[0].src;
              } else {
                image = item.image;
              }
              if (!item.URL) {
                URL = `/view/${item.slug}`;
              } else {
                URL = item.URL;
              }
              return (
                <div className={'col-md-' + col} key={index}>
                  <div className={styles.newsItem}>
                    <a href={URL}>
                      <img src={image} />
                    </a>
                    <span>{item.title}</span>
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}
