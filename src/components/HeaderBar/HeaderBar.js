import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect, pushState} from 'react-redux';
import {load} from 'redux/modules/info';
import {Intro} from 'containers';

const styles = require('./HeaderBar.scss');
const logoImage = require('../../../static/logo.png');
const logoImage2 = require('../../../static/logo2.png');

@connect(
    state => ({info: state.info.data, setting: state.setting.isSetting}),
    dispatch => bindActionCreators({load, pushState}, dispatch))
export default class HeaderBar extends Component {
  static propTypes = {
    info: PropTypes.object,
    load: PropTypes.func.isRequired,
    pushState: PropTypes.func,
    setting: PropTypes.object
  }

  state = {
    isActiveChooseLang: false,
    isToggle_1: false,
    isToggle_2: false,
  }

  _isActiveChooseLang() {
    this.setState({
      isActiveChooseLang: !this.state.isActiveChooseLang
    });
  }

  linkURL(link) {
    this.props.pushState(null, link);
  }

  _toogleA() {
    this.setState({ isToggle_1: !this.state.isToggle_1, isToggle_2: false });
  }
  _toogleB() {
    this.setState({ isToggle_2: !this.state.isToggle_2, isToggle_1: false });
  }
  render() {
    const { setting: isLang } = this.props;

    return (
      <div className={styles.headerBar}>
        <input type="checkbox" id="offcanvas-menu" className="toggle" />
        <aside className="menu-container">
          <div className="menu-heading clearfix">
            <label htmlFor="offcanvas-menu" className="close-btn">
              <i className="fa fa-times"></i>
            </label>
          </div>

          <nav className="slide-menu">
            <a href="/" ><img src={logoImage2} className={'img-responsive'} /></a>
            <ul className={'list-unstyled'}>
              <li><a href="/">{ isLang.HOME }</a></li>
              <li><a href="/publication-awards/PUBLICATIONS_AWARDS">{ isLang.PUBLICATIONS_AWARD }</a></li>
              <li className={'haveSubMenu'} onClick={() => this._toogleA()}>{ isLang.WHAT_DO_YOU_WANT_TO_DO }
                { this.state.isToggle_1 &&
                  <ul className={'list-unstyled '}>
                    <li><a href="/purpose/WATERFRONT_MARINAS">{ isLang.WATERFRONT_MARINAS }</a></li>
                    <li><a href="/purpose/MOUNTAIN">{ isLang.MUONTAIN }</a></li>
                    <li><a href="/purpose/TOURISM">{ isLang.TOURISM }</a></li>
                    <li><a href="/purpose/RESIDEENTIAL">{ isLang.RESIDEENTIAL }</a></li>
                    <li><a href="/purpose/PUBLIC_CULTURE">{ isLang.PUBLIC_CULTURE }</a></li>
                    <li><a href="/purpose/PROFESINAL_ACTIVITIES">{ isLang.PROFESINAL_ACTIVITIES }</a></li>
                    <li><a href="/purpose/MIXES_USING">{ isLang.MIXES_USING }</a></li>
                    <li><a href="/purpose/TALL_BUILDING">{ isLang.TALL_BUILDING }</a></li>
                  </ul>
                }
              </li>
              <li className={'haveSubMenu'} onClick={() => this._toogleB()}>{ isLang.WHAT_WE_PROVIDE }
                { this.state.isToggle_2 &&
                  <ul classsName={'list-unstyled'}>
                    <li><a href="/provide/FULL_OPTION_SERVICES">{ isLang.FULL_OPTION_SERVICES }</a></li>
                    <li><a href="/provide/BUSSINESS_INVOVATION">{ isLang.BUSSINESS_INNOVATION }</a></li>
                    <li><a href="/provide/POWER_OF_SYSNERGIES">{ isLang.POWER_OF_SYSNERGIES }</a></li>
                    <li><a href="/provide/CREATIVE_VISION">{ isLang.CREATIVE_VISION }</a></li>
                  </ul>
                }
              </li>
              <li><a href="/knowus">{ isLang.GET_TO_KNOWN_US }</a></li>
              <li><a href="/our-news">{ isLang.OUR_NEWS }</a></li>
              <li><a href="/contact-us">{ isLang.CONTACT_US }</a></li>
              <li onClick={() => this._isActiveChooseLang()}><span>{ isLang.LANGUAGES }</span></li>
            </ul>
          </nav>

        </aside>
        <div className="mark-content">
          <label htmlFor="offcanvas-menu" className="full-screen-close"></label>
        </div>

        <div className={'container'}>

          <div className={'row'}>
            <div className={'col-md-2 col-sm-2 col-xs-2 ' + styles.logo}>
              <a href="/"><img src={logoImage} /></a>
            </div>
            <div className={'col-md-10 hidden-sm hidden-xs'}>
              <div className={'row'}>
                <ul className={'list-inline ' + styles.menu}>
                  <li><a href="/">{ isLang.HOME }</a></li>
                  <li><a href="/publication-awards/PUBLICATIONS_AWARDS">{ isLang.PUBLICATIONS_AWARD }</a></li>
                  <li><a href="/contact-us">{ isLang.CONTACT_US }</a></li>
                  <li onClick={() => this._isActiveChooseLang()}><a href="#">{ isLang.LANGUAGES }</a></li>
                </ul>
              </div>
            </div>

            <div className={'col-xs-10 col-sm-10 hidden-md hidden-lg'}>
              <div className={styles.sideBarMenu}>
                <div className={styles.btn}>
                  <label htmlFor="offcanvas-menu" className="toggle-btn btn-menu">
                    <i className="fa fa-bars"></i>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={'hidden-sm hidden-xs'} style={{'position': 'relative', 'zIndex': '1'}}>
          <div className="row">
            <div className={'col-md-8 col-md-offset-4'}>
              <ul className={'list-inline ' + styles.submenu}>
                <li>{ isLang.WHAT_DO_YOU_WANT_TO_DO }
                      <ul className={'list-unstyled '}>
                        <li><a href="/purpose/WATERFRONT_MARINAS">{ isLang.WATERFRONT_MARINAS } </a></li>
                        <li><a href="/purpose/MOUNTAIN">{ isLang.MUONTAIN } </a></li>
                        <li><a href="/purpose/TOURISM">{ isLang.TOURISM } </a></li>
                        <li><a href="/purpose/RESIDEENTIAL">{ isLang.RESIDEENTIAL } </a></li>
                        <li><a href="/purpose/PUBLIC_CULTURE">{ isLang.PUBLIC_CULTURE } </a></li>
                        <li><a href="/purpose/PROFESINAL_ACTIVITIES">{ isLang.PROFESINAL_ACTIVITIES } </a></li>
                        <li><a href="/purpose/MIXES_USING">{ isLang.MIXES_USING } </a></li>
                        <li><a href="/purpose/TALL_BUILDING">{ isLang.TALL_BUILDING } </a></li>
                      </ul>
                  </li>
                  <li>{ isLang.WHAT_WE_PROVIDE }
                      <ul classsName={'list-unstyled '}>
                        <li><a href="/provide/FULL_OPTION_SERVICES">{ isLang.FULL_OPTION_SERVICES } </a></li>
                        <li><a href="/provide/BUSSINESS_INVOVATION">{ isLang.BUSSINESS_INNOVATION } </a></li>
                        <li><a href="/provide/POWER_OF_SYSNERGIES">{ isLang.POWER_OF_SYSNERGIES } </a></li>
                        <li><a href="/provide/CREATIVE_VISION">{ isLang.CREATIVE_VISION } </a></li>
                      </ul>
                  </li>
                  <li><a href="/knowus">{ isLang.GET_TO_KNOWN_US }</a></li>
                  <li><a href="/our-news">{ isLang.OUR_NEWS }</a></li>
                </ul>
            </div>
          </div>
        </div>
        {this.state.isActiveChooseLang && <Intro btnOpen={this.state.isActiveChooseLang} />}
      </div>
    );
  }
}
