import React, {Component, PropTypes} from 'react';


const styles = require('./TabSector.scss');
export default class TabSector extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    routes: PropTypes.array.isRequired,
    params: PropTypes.object
  };

  render() {
    const {data, params, routes} = this.props;
    return (
      <div className={styles.root}>
          <div className={styles.rTab}>
            <ul className={'list-inline ' + styles.tabMenu}>
              {
                data.map((item, index) => {
                  let url;
                  let isActived;
                  if (params) {
                    if (params.p1 && params.path) {
                      url = '/' + item.type + '/' + params.p1 + '/' + item.url;
                    } else if (params.p1 && !params.path) {
                      url = '/' + item.type + '/' + params.p1 + '/' + item.url;
                    } else {
                      url = '/' + item.type + '/' + item.url;
                    }
                    if (item.url === params.path) {
                      isActived = styles.active;
                    }
                  } else {
                    url = '/' + routes[1].path + '/' + item.url;
                    isActived = '';
                  }
                  return (
                      <li key={index} className={isActived}>
                        <a href={url}> {item.title} </a>
                      </li>
                    );
                })
              }
            </ul>
          </div>
      </div>
    );
  }
}
