/**
 *  Point of contact for component modules
 *
 *  ie: import { CounterButton, InfoBar } from 'components';
 *
 */

export HeaderBar from './HeaderBar/HeaderBar';
export SliderHome from './SliderHome/SliderHome';
export NewsItem from './NewsItem/NewsItem';
export ServicesItem from './ServicesItem/ServicesItem';
export OurSectors from './OurSectors/OurSectors';
export FileItem from './FileItem/FileItem';
export DiscoverItem from './DiscoverItem/DiscoverItem';
export TabSector from './TabSector/TabSector';
export IntroItem from './IntroItem/IntroItem';
export TextImageItem from './TextImageItem/TextImageItem';
export TextItem from './TextItem/TextItem';
export PaginationItem from './PaginationItem/PaginationItem';
export Footer from './Footer/Footer';
