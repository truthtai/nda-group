import React, { Component, PropTypes} from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {SliderHome} from 'components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { view } from 'redux/modules/posts';
import * as _ from 'lodash';
const bb = () => {
  // $('.content-post img').wrapAll('<a href="" data-lightbox="image"></a>');
  const arr = [];
  $('.content-post img').each((index, value) => {
    arr.push(`<a href="${$(value).attr('src')}" data-lightbox="image"></a>`);
    // $(this).wrapAll(contentwrap);
  });
  _.each($('.content-post img'), (img, index) => {
    console.log(index);
    $(`.content-post img:nth-child(${index + 1})`).wrap(arr[index]);
  })
}
@connect(
    state => ({post: state.posts.post}),
    dispatch => bindActionCreators({view}, dispatch)
)

export default class Post extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    post: PropTypes.object,
    view: PropTypes.func
  };

  componentWillMount() {
    this.props.view(this.props.params.id);
  }

  // componentDidMount() {
  //   $('.content-post img').wrapAll('<a href="" data-lightbox="image"></a>');
  // }
  componentDidUpdate() {
    // $('.content-post img').attr('data-lightbox', 'post');
    bb();
  }

  render() {
    const styles = require('./Post.scss');
    const { post } = this.props;
    const images = [];
    if (post) {
      const imagesX = $(post.description).find('img');
      _.map(imagesX, image => {
        images.push({file: image.src})
      });
    }
    return (
      <div className={styles.root}>
        {
          post &&
          <div>
            <Helmet title={post.title} />
            <SliderHome data={images} />

            <div className={'container'}>
              <h3 className={'titleSector text-center'}> {post.title} </h3>
              <div className="content-post">
                <div dangerouslySetInnerHTML={{__html: (post.description || '')}}></div>
              </div>
            </div>
          </div>
        }

      </div>
    );
  }
}
