import React, { Component, PropTypes } from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {NewsItem, ServicesItem, TabSector, SliderHome, IntroItem, TextImageItem} from 'components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { load } from 'redux/modules/posts';
import { loadCategory } from 'redux/modules/category';
import { pushState } from 'redux-router';

@connect(
    state => ({posts: state.posts, category: state.category.selectedCategory}),
    dispatch => bindActionCreators({load, loadCategory, pushState}, dispatch)
)

export default class Purpose extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    posts: PropTypes.object,
    load: PropTypes.func,
    category: PropTypes.object,
    loadCategory: PropTypes.func,
    pushState: PropTypes.func
  };

  componentDidMount() {
    if (!this.props.params.path) {
      this.props.load(this.props.params.p1);
      this.props.loadCategory(this.props.params.p1);
    } else {
      this.props.load(this.props.params.path);
      this.props.loadCategory(this.props.params.path);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.posts.loadedPosts.length < 1 && nextProps.posts.loadedPosts.length === this.props.posts.loadedPosts.length) {
    }
  }

  render() {
    const styles = require('./Purpose.scss');
    const logoImage = require('../../../static/logo.png');
    const { params, routes, posts: { loadedPosts: news }, category } = this.props;

    const items = [
      {
        id: 1,
        img: '/thumbHome/OUR_SPECIALIZED_SERVICES.jpg',
        title: 'Our Specialized Services',
        col: 4,
        url: `${params.p1}_OUR_SPECIALIZED_SERVICES`,
        type: 'purpose',

      }, {
        id: 2,
        img: '/thumbHome/OUR_EXPERTS_PARTNERS.jpg',
        title: 'Our Experts & Partners',
        col: 4,
        url: `${params.p1}_OUR_EXPERTS_PARTNERS`,
        type: 'purpose'
      }, {
        id: 3,
        img: '/thumbHome/OUR_FEATURED_PROJECTS.jpg',
        title: 'Our Featured Projects',
        col: 4,
        url: `${params.p1}_OUR_FEATURED_PROJECTS`,
        type: 'purpose'
      }
    ];

    const intro = {
      id: 1,
      title: category.description,
      img: logoImage
    };

    setTimeout(() => {
      if (this.props.posts.loadedPosts.length < 1) {
        // this.props.pushState(null, '/under-construction');
      }
    }, 3000);
    const defaultlayout = <div><div className={'container'}> { news.length > 0 && <div> <h3 className={'titleSector text-center'}> News </h3> <NewsItem data={news}/> </div> } </div> <div className={'focusBgSector'}> <div className={'container'}> <h3 className={'titleSector'}> Services </h3> <ServicesItem data={items} params={params}/></div> </div></div>;
    const selectedLayout = <div className={'container'}><h3 className={'titleSector text-center'}> Content </h3> { news && <TextImageItem data={news}/> } </div>;
    return (
      <div>
      {
        category &&
        <div className={styles.root}>
          <Helmet title={params.p1.replace(/_/g, ' ')}/>
          <SliderHome data={category.images} />

          <div className={'focusBgSector'}>
            <div className={'container'}>
              <h3 className={'titleSector text-center'}> {params.p1.replace(/_/g, ' ')} </h3>
            </div>
          </div>
          <TabSector data={items} params={params} routes={routes} />
            <div className={'container'}>
              <IntroItem data={intro} />
            </div>
          {
            params.path ? selectedLayout : defaultlayout
          }

        </div>
      }
      </div>
    );
  }
}
