import React, { Component, PropTypes } from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
// import {connect, pushState} from 'react-redux';
import Helmet from 'react-helmet';
import { NewsItem, ServicesItem, OurSectors, SliderHome } from 'components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { load } from 'redux/modules/newsHome';

@connect(
    state => ({newsHome: state.newsHome}),
    dispatch => bindActionCreators({load}, dispatch)
)

export default class Home extends Component {
  static propTypes = {
    newsHome: PropTypes.object,
    load: PropTypes.func
  };

  componentDidMount() {
    this.props.load();
  }

  render() {
    const styles = require('./Home.scss');
    const { newsHome: { data: news } } = this.props;

    const serviceItems = [
      {
        id: 1,
        title: 'Bussiness Innovation',
        img: '/thumbHome/bussiness-innovation.jpg',
        url: 'provide/BUSSINESS_INVOVATION'
      }, {
        id: 2,
        title: 'Power of Sysnergies',
        img: '/thumbHome/power-of-sysnergies.jpg',
        url: 'provide/POWER_OF_SYSNERGIES'
      }, {
        id: 3,
        title: 'Creative Vision',
        img: '/thumbHome/creative-vision.jpg',
        url: 'provide/CREATIVE_VISION'
      }, {
        id: 4,
        title: 'Get to known us',
        img: '/thumbHome/known-us.jpg',
        url: 'provide/knowus'
      }, {
        id: 5,
        title: 'Full option services',
        img: '/thumbHome/full-option-services.jpg',
        col: 8,
        url: 'provide/FULL_OPTION_SERVICES'
      }

    ];

    return (
      <div className={styles.home}>
        <Helmet title="Home" />
        <SliderHome />

        { news &&
          <div className={'container'}>
              <h3 className={'titleSector'}>News </h3>
              <NewsItem data={news} />
          </div>
        }

        <div className={'focusBgSector'}>
          <div className={'container'}>
            <h3 className={'titleSector'}>Services </h3>
            <ServicesItem data={serviceItems} />
          </div>
        </div>


        <div className={'container'}>
          <h3 className={'titleSector'}>Our sectors </h3>
          <OurSectors />
        </div>
      </div>
    );
  }
}
