import React, { Component, PropTypes } from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {FileItem, DiscoverItem, SliderHome, IntroItem, PaginationItem} from 'components';
export default class CreativeVision extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./CreativeVision.scss');
    const logoImage = require('../../../static/logo.png');
    const intro = {
      id: 1,
      title: 'With a proven track record of 80 marine-related studies and waterfront projects, NDA Group has been a key contributor to the yachting industry by bringing new waterfront experiences around the world and especially in Asia.',
      img: logoImage
    };

    const items = [
      {
        id: 1,
        img: 'http://static1.squarespace.com/static/524b5395e4b0ad97e93a5b0f/537c84fde4b0bb0d036df739/537c8505e4b0bb0d036df73c/1406801590590/Sanya+Luhuidou+2.jpg?format=750w',
        title: 'SANYA LUHUITOU PLAZA TOURIST CENTRE',
        col: 4,
        url: 'sanya-luhuitou-plaza-tourist-centre'
      }, {
        id: 2,
        img: 'http://static1.squarespace.com/static/524b5395e4b0ad97e93a5b0f/53a7dba1e4b088db7ad282d5/53a7dba6e4b0a2a8b8847188/1403509695612/v1_c1_small.jpg?format=750w',
        title: 'DALIAN EAST PORT YACHT CLUBS',
        col: 4,
        url: 'dalian-east-port-yacht-clubs'
      }, {
        id: 3,
        img: 'http://static1.squarespace.com/static/524b5395e4b0ad97e93a5b0f/53a7d272e4b0dd65b7925200/53a7d287e4b0dd65b792520c/1403507342697/080905-jd01_small.jpg?format=500w',
        title: 'SANYA AGILE CLEARWATER BAY',
        col: 4,
        url: 'sanya-agile-clearwater-bay'
      }, {
        id: 4,
        img: 'http://static1.squarespace.com/static/524b5395e4b0ad97e93a5b0f/53a7ce38e4b01786c92224b1/53a7ce3be4b057f6687a15e0/1403506239832/view1%2Bversion-A_small.jpg?format=500w',
        title: 'BAHRAIN DURRAT MARINA',
        col: 4,
        url: 'bahrain-durrat-marina'
      }

    ];

    const files = [
      {
        id: 1,
        title: 'NDA Vietnam Portfolio 2016 - Resort and Island',
        file: 'https://www.dropbox.com/s/00uaf18ayh6g3gz/NDA%20Vietnam%20Portfolio%202016%20-%20Resort%20and%20Island.pdf?dl=1'
      }, {
        id: 2,
        title: 'NDA Vietnam Portfolio 2016 - General',
        file: 'https://www.dropbox.com/s/jf3kqdbtzv71eu9/NDA%20Vietnam%20Portfolio%202016%20-%20General.pdf?dl=1'
      }
    ];

    const paginationData = {
      items: 20,
      maxBtn: 5,
      current: 1
    };
    return (
      <div className={styles.root}>
        <Helmet title="Creative Vision"/>
        <SliderHome />

        <div className={'container'}>
          <h3 className={'titleSector'}> Creative Vision </h3>
          <IntroItem data={intro} />
        </div>

        <div className={'container'}>
          <h3 className={'titleSector'}> Projects </h3>
          <DiscoverItem data={items}/>
          <PaginationItem data={paginationData} />
        </div>

        <div className={'focusBgSector'}>
          <FileItem data={files}/>
        </div>

      </div>
    );
  }
}
