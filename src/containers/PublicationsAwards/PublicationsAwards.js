import React, { Component, PropTypes } from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {TabSector, SliderHome, IntroItem, TextImageItem, DiscoverItem} from 'components';
import { load } from 'redux/modules/posts';
import { pushState } from 'redux-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loadCategory } from 'redux/modules/category';

@connect(
    state => ({posts: state.posts, category: state.category.selectedCategory}),
    dispatch => bindActionCreators({load, pushState, loadCategory}, dispatch)
)

export default class PublicationsAwards extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    pushState: PropTypes.func,
    posts: PropTypes.object,
    load: PropTypes.func,
    loadCategory: PropTypes.func,
    category: PropTypes.object
  };

  componentDidMount() {
    this.props.load(this.props.params.p1);
    this.props.loadCategory(this.props.params.p1);
  }

  render() {
    const styles = require('./PublicationsAwards.scss');
    const logoImage = require('../../../static/logo.png');
    const {params, routes, posts: { loadedPosts: news }, category } = this.props;
    // console.log(this.props);
    // const items = [
    //   {
    //     id: 1,
    //     img: 'https://image.webservices.ft.com/v1/images/raw/https%3A%2F%2Flive.ft.com%2Fvar%2Fftlive%2Fstorage%2Fimages%2Fevents%2Fwebcast-the-new-vision-for-value-in-asset-management%2F688158-11-eng-GB%2FWebcast-The-New-Vision-for-Value-in-Asset-Management.jpg?source=ftlive&format=jpg&width=1024',
    //     title: 'Brochures',
    //     col: 4,
    //     url: 'brochures',
    //     type: 'publications-awards',
    //
    //   }, {
    //     id: 2,
    //     img: 'https://s3.amazonaws.com/wordpress-production/wp-content/uploads/2014/09/Team-Building-Activities-Redbooth-v2.png',
    //     title: 'Case Studies and White Papers',
    //     col: 4,
    //     url: 'case-studies-and-white-papers',
    //     type: 'publications-awards'
    //   }, {
    //     id: 3,
    //     img: 'http://cdn1.theodysseyonline.com/files/2015/12/04/635847974891062780-425303270_news.jpg',
    //     title: 'Publications and Press Releases',
    //     col: 4,
    //     url: 'publications-and-press-releases',
    //     type: 'publications-awards'
    //   }, {
    //     id: 4,
    //     img: 'http://cdn1.theodysseyonline.com/files/2015/12/04/635847974891062780-425303270_news.jpg',
    //     title: 'Awards',
    //     col: 4,
    //     url: 'awards',
    //     type: 'publications-awards'
    //   }
    // ];

    const items = [
      {
        id: 1,
        img: '/thumbHome/OUR_SPECIALIZED_SERVICES.jpg',
        title: 'BROCHURES',
        col: 4,
        url: `${params.p1}_BROCHURES`,
        type: 'publications-awards',

      }, {
        id: 2,
        img: '/thumbHome/OUR_EXPERTS_PARTNERS.jpg',
        title: 'CASE STUDIES AND WHITE PAPERS',
        col: 4,
        url: `${params.p1}_CASE_STUDIES_AND_WHITE_PAPERS`,
        type: 'publications-awards'
      }, {
        id: 3,
        img: '/thumbHome/OUR_FEATURED_PROJECTS.jpg',
        title: 'PUBLICATIONS AND PRESS RELEASES',
        col: 4,
        url: `${params.p1}_PUBLICATIONS_AND_PRESS_RELEASES`,
        type: 'publications-awards'
      }, {
        id: 3,
        img: '/thumbHome/OUR_FEATURED_PROJECTS.jpg',
        title: 'AWARDS',
        col: 4,
        url: `${params.p1}_AWARDS`,
        type: 'publications-awards'
      }
    ];


    setTimeout(() => {
      if (news.loadedPosts.length < 1) {
        // this.props.pushState(null, '/under-construction');
      }
    }, 3000);
    const intro = {
      id: 1,
      title: category.description,
      img: logoImage
    };

    const defaultlayout = <div><div className={'container'}> <h3 className={'titleSector'}> {params.p1 ? params.p1 : 'Publications & Awards'} </h3><TabSector data={items} params={params} routes={routes}/><IntroItem data={intro} /> </div> <div className={'focusBgSector'}> <div className={'container'}> <h3 className={'titleSector'}> Services </h3> { params.p1 ? <TextImageItem data={news}/> : <DiscoverItem data={items} params={params}/> } </div> </div></div>;
    const selectedLayout = <div className={'container'}><h3 className={'titleSector text-center'}> Content </h3> { news && <TextImageItem data={news}/> } </div>;
    return (
        <div>
        {
          category &&
          <div className={styles.root}>
            <Helmet title={params.p1.replace(/_/g, ' ')}/>
            <SliderHome data={category.images} />

            <div className={'focusBgSector'}>
              <div className={'container'}>
                <h3 className={'titleSector text-center'}> {params.p1.replace(/_/g, ' ')} </h3>
              </div>
            </div>
              <div className={'container'}>
                <TabSector data={items} params={params} routes={routes} />
                <IntroItem data={intro} />
              </div>
            {
              params.path ? selectedLayout : defaultlayout
            }

          </div>
        }
        </div>
      );
  }
}
