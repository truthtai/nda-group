import React, { Component, PropTypes} from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {NewsItem, TabSector, SliderHome, PaginationItem} from 'components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { load } from 'redux/modules/posts';

@connect(
    state => ({posts: state.posts}),
    dispatch => bindActionCreators({load}, dispatch)
)

export default class News extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    posts: PropTypes.object,
    load: PropTypes.func
  };

  componentDidMount() {
    this.props.load('NEWS');
  }

  render() {
    const styles = require('./News.scss');
    const {routes, params, posts: { loadedPosts: news } } = this.props;
    const items = [
      {
        id: 1,
        img: 'https://image.webservices.ft.com/v1/images/raw/https%3A%2F%2Flive.ft.com%2Fvar%2Fftlive%2Fstorage%2Fimages%2Fevents%2Fwebcast-the-new-vision-for-value-in-asset-management%2F688158-11-eng-GB%2FWebcast-The-New-Vision-for-Value-in-Asset-Management.jpg?source=ftlive&format=jpg&width=1024',
        title: 'Vision & Values',
        url: 'vision-values',
        type: 'knowus'
      }, {
        id: 2,
        img: 'https://s3.amazonaws.com/wordpress-production/wp-content/uploads/2014/09/Team-Building-Activities-Redbooth-v2.png',
        title: 'Team',
        url: 'team',
        type: 'knowus'
      }, {
        id: 3,
        img: 'http://cdn1.theodysseyonline.com/files/2015/12/04/635847974891062780-425303270_news.jpg',
        title: 'News',
        url: 'news',
        type: 'knowus'
      }, {
        id: 4,
        img: 'http://www.timewarner.com/sites/timewarner.com/files/careers/twc-careers-slide-2.jpg',
        title: 'Careers',
        url: 'careers',
        type: 'knowus'
      }
    ];

    const paginationData = {
      items: 20,
      maxBtn: 5,
      current: 1
    };

    return (
      <div className={styles.root}>
        <Helmet title="KnowUs"/>
        <SliderHome />

        <div className={'container'}>
          <h3 className={'titleSector text-center'}> Get know us </h3>
        </div>
        <TabSector data={items} params={params} routes={routes} />
        {
          news.length > 0 &&
          <div className={'container'}>
            <h3 className={'titleSector'}> News </h3>
            <NewsItem data={news} />
            <PaginationItem data={paginationData} />
          </div>
        }
      </div>
    );
  }
}
