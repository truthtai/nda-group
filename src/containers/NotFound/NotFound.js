import React, {Component} from 'react';
const styles = require('./NotFound.scss');
const bg = require('./NotFound.jpg');

export default class NotFound extends Component {
	state = {
  viewFile: ''
	}

  _viewFile(file) {
    this.setState({
      viewFile: file
    });
  }
  _resetViewFile() {
    this.setState({
      viewFile: null
    });
  }

  _fullScreen() {
    const viewer = document.getElementById('viewer');
    const rFS = viewer.mozRequestFullScreen || viewer.webkitRequestFullscreen || viewer.requestFullscreen;
    rFS.call(viewer);
  }

  render() {
    return (
  	<div className={styles.bg} style={{'background': `url("${bg}")`}}>
  		<div className={styles.root}>
				<div className="container">
					<h1>OUR WEBSITE IS UNDER CONSTRUCTION</h1>
					<div className="col-md-6 col-md-offset-3">
						<div className={styles.subr}>
							<input defaultValue="" placeholder="Your email will subscribe" />
							<a className={'btn btn-success'}> Subscribe </a>
						</div>
					</div>
					<div className={styles.downloadProfile}>
						<div className={'col-md-12'}><h4> Portfolio </h4></div>
						<div className={'col-md-3 col-md-offset-3 '}>
							<span className={styles.btnDownload}>NDA Vietnam Portfolio 2016 - Resort and Island</span>
							<ul className={'list-unstyled ' + styles.optionProfile}>
								<li>
									<a href="https://www.dropbox.com/s/00uaf18ayh6g3gz/NDA%20Vietnam%20Portfolio%202016%20-%20Resort%20and%20Island.pdf?dl=1">Download</a>
								</li>

								<li onClick={() => this._viewFile('00uaf18ayh6g3gz/NDA%20Vietnam%20Portfolio%202016%20-%20Resort%20and%20Island.pdf')} className={this.state.viewFile === '0B76vLvRCIEvYcThvcmd2bzFyY00' ? styles.active : ''}>
									<a>View Online</a>
								</li>
							</ul>
						</div>

						<div className={'col-md-3'}>
							<span className={styles.btnDownload}>NDA Vietnam Portfolio 2016 - General</span>
							<ul className={'list-unstyled ' + styles.optionProfile}>
								<li>
									<a href="https://www.dropbox.com/s/jf3kqdbtzv71eu9/NDA%20Vietnam%20Portfolio%202016%20-%20General.pdf?dl=1">Download</a>
								</li>

								<li onClick={() => this._viewFile('jf3kqdbtzv71eu9/NDA%20Vietnam%20Portfolio%202016%20-%20General.pdf')} className={this.state.viewFile === '0B76vLvRCIEvYQzBnajU2ZUsydkk' ? styles.active : ''}>
									<a>View Online</a>
								</li>
							</ul>
						</div>

						<div className={'col-md-3 col-md-offset-3 '}>
							<span className={styles.btnDownload}>NDA Vietnam Portfolio 2016 - Marina & Waterfront</span>
							<ul className={'list-unstyled ' + styles.optionProfile}>
								<li>
									<a href="https://www.dropbox.com/s/4hhedb8alle30kx/NDA%20Vietnam%20Portfolio%202016%20-%20Marina%20%26%20Waterfront.pdf?dl=1">Download</a>
								</li>

								<li onClick={() => this._viewFile('4hhedb8alle30kx/NDA%20Vietnam%20Portfolio%202016%20-%20Marina%20%26%20Waterfront.pdf')} className={this.state.viewFile === '0B76vLvRCIEvYcThvcmd2bzFyY00' ? styles.active : ''}>
									<a>View Online</a>
								</li>
							</ul>
						</div>

						<div className={'col-md-3'}>
							<span className={styles.btnDownload}>NDA Vietnam Portfolio 2016 - Waterfront & tourism mini brochure</span>
							<ul className={'list-unstyled ' + styles.optionProfile}>
								<li>
									<a href="https://www.dropbox.com/s/y1zl6jq3w12smyf/NDA%20Vietnam%20Portfolio%202016%20-%20Waterfront%20%26%20tourism%20mini%20brochure.pdf?dl=0">Download</a>
								</li>

								<li onClick={() => this._viewFile('y1zl6jq3w12smyf/NDA%20Vietnam%20Portfolio%202016%20-%20Waterfront%20%26%20tourism%20mini%20brochure.pdf')} className={this.state.viewFile === '0B76vLvRCIEvYcThvcmd2bzFyY00' ? styles.active : ''}>
									<a>View Online</a>
								</li>
							</ul>
						</div>

					</div>
					{
					this.state.viewFile &&
					<div className={styles.viewOnline}>
						<div className={styles.close}>
							<a onClick={() => this._fullScreen()}> FULL SCREEN </a>
							<a onClick={() => this._resetViewFile()}> CLOSE </a>
						</div>
						<div className={'iframeSrc'}>
							<iframe id="viewer" src={'http://azu.github.io/slide-pdf.js/?slide=https://dl.dropboxusercontent.com/s/' + this.state.viewFile + '?raw=1'} width="100%" height="100%" frameBorder="0">
							</iframe>
						</div>
					</div>
					}
				</div>
			</div>
		</div>
  );
  }
}
