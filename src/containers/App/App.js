import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
// import { IndexLink } from 'react-router';
import Helmet from 'react-helmet';
// import { isLoaded as isInfoLoaded, load as loadInfo } from 'redux/modules/info';
// import { isLoaded as isAuthLoaded, load as loadAuth, logout } from 'redux/modules/auth';
import { pushState } from 'redux-router';
// import connectData from 'helpers/connectData';
import config from '../../config';
import {HeaderBar, Footer} from 'components';
import { getSetting } from '../../redux/modules/setting';
import { setLang } from '../../redux/modules/lang';

// function fetchData(getState, dispatch) {
//   const promises = [];
//   // if (!isInfoLoaded(getState())) {
//   //   promises.push(dispatch(loadInfo()));
//   // }
//   if (!isLoadedSetting(getState())) {
//     promises.push(dispatch(getSetting(getState().lang)));
//   }
//   return Promise.all(promises);
// }
//
// @connectData(fetchData)
@connect(
  state => ({user: state.auth.user, lang: state.lang.isLang, setting: state.setting.isSetting}),
  {pushState, setLang, getSetting})
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    user: PropTypes.object,
    pushState: PropTypes.func.isRequired,
    lang: PropTypes.string,
    setLang: PropTypes.func,
    setting: PropTypes.object,
    getSetting: PropTypes.func
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentDidMount() {
    const lang = localStorage.getItem('nda_lang') || 'en_EU';
    console.log(lang);
    this.props.setLang(lang);
    this.props.getSetting(lang);
  }

  componentWillReceiveProps() {
  }


  checkLang() {
    return localStorage.getItem('nda_lang');
  }

  render() {
    const styles = require('./App.scss');

    return (
      <div className={styles.app}>
        <Helmet {...config.app.head}/>
        <HeaderBar />
        <div className={styles.appContent}>
          <section className={'content'}>
            {this.props.children}
          </section>
        </div>
        <Footer />
      </div>
    );
  }
}
