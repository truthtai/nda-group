import React, { Component, PropTypes } from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {TabSector, SliderHome, IntroItem, TextImageItem} from 'components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { load } from 'redux/modules/posts';
import { loadCategory } from 'redux/modules/category';
import { pushState } from 'redux-router';
@connect(
    state => ({posts: state.posts, category: state.category.selectedCategory}),
    dispatch => bindActionCreators({load, loadCategory, pushState}, dispatch)
)

export default class Provide extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    posts: PropTypes.object,
    load: PropTypes.func,
    loadCategory: PropTypes.func,
    category: PropTypes.object,
    pushState: PropTypes.func
  };

  componentDidMount() {
    this.props.load(this.props.params.p1);
    this.props.loadCategory(this.props.params.p1);
  }

  render() {
    const styles = require('./Provide.scss');
    const logoImage = require('../../../static/logo.png');
    const {params, routes, posts: { loadedPosts: news }, category} = this.props;
    // console.log(this.props);
    const items = [
      {
        id: 1,
        img: 'https://image.webservices.ft.com/v1/images/raw/https%3A%2F%2Flive.ft.com%2Fvar%2Fftlive%2Fstorage%2Fimages%2Fevents%2Fwebcast-the-new-vision-for-value-in-asset-management%2F688158-11-eng-GB%2FWebcast-The-New-Vision-for-Value-in-Asset-Management.jpg?source=ftlive&format=jpg&width=1024',
        title: 'Our Services',
        col: 4,
        url: 'our-services',
        type: 'provide',

      }, {
        id: 2,
        img: 'https://s3.amazonaws.com/wordpress-production/wp-content/uploads/2014/09/Team-Building-Activities-Redbooth-v2.png',
        title: 'Our Sector',
        col: 4,
        url: 'our-sector',
        type: 'provide'
      }, {
        id: 3,
        img: 'http://cdn1.theodysseyonline.com/files/2015/12/04/635847974891062780-425303270_news.jpg',
        title: 'Testimonies',
        col: 4,
        url: 'testimonies',
        type: 'provide'
      }, {
        id: 4,
        img: 'http://cdn1.theodysseyonline.com/files/2015/12/04/635847974891062780-425303270_news.jpg',
        title: 'Insights',
        col: 4,
        url: 'insights',
        type: 'provide'
      }
    ];

    const intro = {
      id: 1,
      title: category.description,
      img: logoImage
    };

    setTimeout(() => {
      if (this.props.posts.loadedPosts.length < 1) {
        // this.props.pushState(null, '/under-construction');
      }
    }, 3000);
    const defaultlayout = <div className={'container'}><h3 className={'titleSector text-center'}> Content </h3> { news.length > 0 && <TextImageItem data={news}/> } </div>;
    // const selectedLayout = <div className={'container'}><h3 className={'titleSector'}> Content </h3><TextImageItem data={news}/></div>;
    return (
      <div>
      {
        category &&
      <div className={styles.root}>
        <Helmet title={params.p1.replace(/_/g, ' ')}/>
        <SliderHome data={category.images} />

        <div className={'container'}>
          <h3 className={'titleSector text-center'}> {params.p1.replace(/_/g, ' ')} </h3>
        </div>
        { params.p1 === 'bussiness-invovation' && <TabSector data={items} params={params} routes={routes} /> }
        <div className={'container'}>
          <IntroItem data={intro} />
        </div>
        {
          defaultlayout
        }

      </div>
      }
    </div>
    );
  }
}
