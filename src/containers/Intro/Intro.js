import React, {Component, PropTypes} from 'react';
import Helmet from 'react-helmet';
const styles = require('./Intro.scss');
const bg = require('./worldmap.png');
const logoImage = require('../../../static/logo.png');
import { pushState } from 'redux-router';
import { connect } from 'react-redux';
import { setLang } from '../../redux/modules/lang';
import { getSetting } from '../../redux/modules/setting';
@connect(() => ({}),
  {pushState, setLang, getSetting}
)

export default class Info extends Component {

  static propTypes = {
    btnOpen: PropTypes.bool,
    pushState: PropTypes.func.isRequired,
    setLang: PropTypes.func,
    getSetting: PropTypes.func
  }

  state = {
    isOpen: this.props.btnOpen || true
  }
  _open() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  _choose(lang) {
    localStorage.setItem('nda_lang', lang);
    this.props.setLang(lang);
    this.props.getSetting(lang);
    this.props.pushState(null, '/home');
    this._open();
  }

  render() {
    return (
  <div>
  <Helmet title="Home" />
  {
  	this.state.isOpen &&
  <div className={styles.intro}>
  	{ this.props.btnOpen && <div className={styles.close}>
  		<a onClick={() => this._open()}> <span> <i className={'fa fa-close fa-4x'}> </i> </span> </a>
  	</div>
    }
  	<div className={styles.bg} style={{'background': `url("${bg}")`}}>
  		<div className={styles.root}>
				<div className="container">
					<a href="/" ><img src={logoImage} className={'img-responsive ' + styles.logo} /></a>
					<div className={styles.lang}>
            <ul className="list-unstyled">
                <li className={styles.content}>
                  <span className={'col-md-12 col-xs-12 col-sm-12 ' + styles.region}>Europe</span>
                  <div className={'col-md-6 col-xs-6 col-sm-6 ' + styles.div} onClick={() => this._choose('fr_EU')} >
                    <a className={styles.langName}><img src="/lang/FR.png" alt="" />France </a>
                  </div>

                  <div className={'col-md-6 col-xs-6 col-sm-6 ' + styles.div} onClick={() => this._choose('en_EU')}>
                    <a className={styles.langName}><img src="/lang/GB.png" alt="" />English</a>
                  </div>
                </li>
                <li className={styles.content}>
                  <span className={'col-md-12 col-xs-12 col-sm-12 ' + styles.region}>CHINA</span>
                  <div className={'col-md-6 col-xs-6 col-sm-6 ' + styles.div} onClick={() => this._choose('cn_CN')}>
                    <a href="#" className={styles.langName}><img src="/lang/CN.png" alt="" />Chinese </a>
                  </div>

                  <div className={'col-md-6 col-xs-6 col-sm-6 ' + styles.div} onClick={() => this._choose('en_CN')}>
                    <a className={styles.langName}><img src="/lang/GB.png" alt="" />English</a>
                  </div>
                </li>

                <li className={styles.content}>
                  <span className={'col-md-12 col-xs-12 col-sm-12 ' + styles.region}>VIETNAM</span>
                  <div className={'col-md-6 col-xs-6 col-sm-6 ' + styles.div} onClick={() => this._choose('vi_VN')}>
                    <a className={styles.langName}><img src="/lang/VN.png" alt="" />Vietnamese </a>
                  </div>

                  <div className={'col-md-6 col-xs-6 col-sm-6 ' + styles.div} onClick={() => this._choose('en_VN')}>
                    <a className={styles.langName}><img src="/lang/GB.png" alt="" />English</a>
                  </div>
                </li>

              </ul>
          </div>
				</div>
			</div>
		</div>
	</div>
	}
		</div>
	);
  }
}
