import React, { Component, PropTypes} from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {FileItem, DiscoverItem, TabSector, SliderHome} from 'components';
export default class KnowUs extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./KnowUs.scss');
    const {routes, params} = this.props;
    const items = [
      {
        id: 1,
        img: 'https://image.webservices.ft.com/v1/images/raw/https%3A%2F%2Flive.ft.com%2Fvar%2Fftlive%2Fstorage%2Fimages%2Fevents%2Fwebcast-the-new-vision-for-value-in-asset-management%2F688158-11-eng-GB%2FWebcast-The-New-Vision-for-Value-in-Asset-Management.jpg?source=ftlive&format=jpg&width=1024',
        title: 'Vision & Values',
        url: 'vision-values',
        type: 'knowus'
      }, {
        id: 2,
        img: 'https://s3.amazonaws.com/wordpress-production/wp-content/uploads/2014/09/Team-Building-Activities-Redbooth-v2.png',
        title: 'Team',
        url: 'team',
        type: 'knowus'
      }, {
        id: 3,
        img: 'http://cdn1.theodysseyonline.com/files/2015/12/04/635847974891062780-425303270_news.jpg',
        title: 'News',
        url: 'news',
        type: 'knowus'
      }, {
        id: 4,
        img: 'http://www.timewarner.com/sites/timewarner.com/files/careers/twc-careers-slide-2.jpg',
        title: 'Careers',
        url: 'careers',
        type: 'knowus'
      }
    ];

    const files = [
      {
        id: 1,
        title: 'NDA Vietnam Portfolio 2016 - Resort and Island',
        file: 'https://www.dropbox.com/s/00uaf18ayh6g3gz/NDA%20Vietnam%20Portfolio%202016%20-%20Resort%20and%20Island.pdf?dl=1'
      }, {
        id: 2,
        title: 'NDA Vietnam Portfolio 2016 - General',
        file: 'https://www.dropbox.com/s/jf3kqdbtzv71eu9/NDA%20Vietnam%20Portfolio%202016%20-%20General.pdf?dl=1'
      }
    ];
    return (
      <div className={styles.root}>
        <Helmet title="KnowUs"/>
        <SliderHome />
        <div className={'focusBgSector'}>
          <div className={'container'}>
            <h3 className={'titleSector text-center'}> Get to know Us </h3>
          </div>
        </div>
        <TabSector data={items} params={params} routes={routes} />
        <div className="container">
          <DiscoverItem data={items} params={params}/>
        </div>
        <div className={'focusBgSector'}>
            <FileItem data={files}/>
        </div>
      </div>
    );
  }
}
