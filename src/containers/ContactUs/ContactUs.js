import React, { Component, PropTypes} from 'react';
// import { Link } from 'react-router';
// import config from '../../config';
import Helmet from 'react-helmet';
import {SliderHome} from 'components';
export default class ContactUs extends Component {
  static propTypes = {
    routes: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./ContactUs.scss');
    // const {routes, params} = this.props;
    const ourOffice = [
      {
        country: 'CHINA',
        address: 'Rm 601, Bldg C, 525 Fahuazhen Rd. Shanghai 200052, China',
        phone: '+86 (0)21 6082 5482',
        fax: '+86 (0)21 6082 5486',
        email: 'contact@ndateam.com'
      }, {
        country: 'FRANCE',
        address: '8 Cours Lafayette, 69003 Lyon, France',
        phone: '+33 (0)4 2629 0800',
        fax: '+33 (0)4 2629 0801',
        email: 'contact@ndateam.com'
      }, {
        country: 'UAE',
        address: 'Office 1601 , Jafza View 18 P .O . Box 18271 Dubai, UAE',
        phone: '+971 (0)4886 5690',
        fax: '+971 (0)4886 5691',
        email: 'contact@ndateam.com'
      }, {
        country: 'VIETNAM',
        address: 'Room 502, Thanh Do Tower, 16 Binh Loi St., Ward 13, Binh Thanh Dist, HCM City, Vietnam',
        phone: '+84 (8) 6250 3300',
        fax: '+84 (8) 6258 3821',
        email: 'chinh.dang@ndagroup.com.vn',
        hp: '+84 1234 668 779'
      }
    ];

    return (
      <div className={styles.root}>
        <Helmet title="Our Offices"/>
        <SliderHome />

        <div className={'container'}>
          <h3 className={'titleSector'}> Our Offices </h3>
        </div>
        <div className={'container'}>
          <div className="row">
            {
              ourOffice.map((info, index) => {
                return (
                    <div className={'col-md-3 ' + styles.ourOffice} key={index}>
                      <p className={styles.country}>{info.country}</p>
                      <p><strong>A</strong> {info.address}</p>
                      <p><strong>P</strong> {info.phone}</p>
                      <p><strong>F</strong> {info.fax}</p>
                      <p><strong>E</strong> {info.email}</p>
                      { info.hp && <p><strong>HP</strong> {info.hp}</p> }
                    </div>
                  );
              })
            }
          </div>
        </div>

        <div className="focusBgSector">
        <div className={styles.contactForm}>

          <div className="container">
            <h3 className="titleSector"> CONTACT FORM </h3>
            <div className="row">
              <div className="col-md-6">
                <h4 className={styles.active}> Proposal for services </h4>
                </div>
                <div className="col-md-6">
                  <h4> For any other question </h4>
              </div>
            </div>

            <h5>What about you?</h5>
            <form className={'form-horizontal ' + styles.form} role="form">
              <div className="form-group">
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="First Name" />
                </div>
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Last Name" />
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Position" />
                </div>
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Company" />
                </div>
              </div>


              <div className="form-group">
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Email" />
                </div>
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Phone Number" />
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Country" />
                </div>
                <div className="col-sm-6">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Additional information" />
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-12">
                  <input className="form-control" id="focusedInput" type="text" placeholder="Title" />
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-12">
                  <textarea className="form-control" id="focusedInput" type="text" rows="10" placeholder="Content" />
                </div>
              </div>

              <a className="btn btn-block btn-primary"> SUBMIT </a>
            </form>
          </div>
        </div>
        </div>
      </div>
    );
  }
}
