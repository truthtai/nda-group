import { combineReducers } from 'redux';
import multireducer from 'multireducer';
import { routerStateReducer } from 'redux-router';

import auth from './auth';
import counter from './counter';
import { reducer as form } from 'redux-form';
import info from './info';
import lang from './lang';
import newsHome from './newsHome';
import setting from './setting';
import posts from './posts';
import category from './category';

export default combineReducers({
  router: routerStateReducer,
  auth,
  form,
  newsHome,
  posts,
  multireducer: multireducer({
    counter1: counter,
    counter2: counter,
    counter3: counter
  }),
  info,
  lang,
  setting,
  category
});
