const LOAD = 'redux-example/posts/LOAD';
const LOAD_SUCCESS = 'redux-example/posts/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/posts/LOAD_FAIL';

const LOAD_POST = 'redux-example/posts/LOAD_POST';
const LOAD_POST_SUCCESS = 'redux-example/posts/LOAD_POST_SUCCESS';
const LOAD_POST_FAIL = 'redux-example/posts/LOAD_POST_FAIL';

const initialState = {
  gettingPosts: false,
  loadedPosts: [],
  errorPosts: false,
  gettingPost: false,
  post: null,
  errorPost: null,
};

export default function posts(state = initialState, action = {}) {
  switch (action.type) {
  case LOAD:
    return {
      ...state,
      gettingPosts: true
    };
  case LOAD_SUCCESS:
    return {
      ...state,
      loadedPosts: action.result,
      gettingPosts: false
    };
  case LOAD_FAIL:
    return {
      ...state,
      errorPosts: action.err
    };
  case LOAD_POST:
    return {
      ...state,
      gettingPost: true
    };
  case LOAD_POST_SUCCESS:
    return {
      ...state,
      post: action.result,
      gettingPost: false
    };
  case LOAD_POST_FAIL:
    return {
      ...state,
      errorPost: action.err
    };

  default:
    return state;
  }
}

export function isLoaded(globalState) {
  return globalState.setting && globalState.setting.loadedPosts;
}

export function load(catName) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(`/posts?id=${catName}`)
  };
}

export function view(id) {
  return {
    types: [LOAD_POST, LOAD_POST_SUCCESS, LOAD_POST_FAIL],
    promise: (client) => client.get(`/view?id=${id}`)
  };
}
