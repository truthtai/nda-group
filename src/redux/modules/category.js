const LOAD = 'redux-example/category/LOAD';
const LOAD_SUCCESS = 'redux-example/category/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/category/LOAD_FAIL';

const initialState = {
  gettingCategory: false,
  selectedCategory: {},
  error: false
};

export default function category(state = initialState, action = {}) {
  switch (action.type) {
  case LOAD:
    return {
      ...state,
      gettingCategory: true
    };
  case LOAD_SUCCESS:
    return {
      ...state,
      selectedCategory: action.result,
      gettingCategory: false
    };
  case LOAD_FAIL:
    return {
      ...state,
      error: action.err
    };

  default:
    return state;
  }
}

export function isLoaded(globalState) {
  return globalState.setting && globalState.setting.loaded;
}

export function loadCategory(catName) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(`/category?name=${catName}`)
  };
}
