const LOAD = 'redux-example/lang/LOAD';

const initialState = {
  isLang: '',
};

export default function lang(state = initialState, action = {}) {
  switch (action.type) {
  case LOAD:
    return {
      ...state,
      isLang: action.lang
    };

  default:
    return state;
  }
}

export function setLang(langUse) {
  return {
    type: LOAD,
    lang: langUse
  };
}
