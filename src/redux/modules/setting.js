const LOAD = 'redux-example/setting/LOAD';
const LOAD_SUCCESS = 'redux-example/setting/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/setting/LOAD_FAIL';

const LOAD_SOCIAL = 'redux-example/setting/LOAD_SOCIAL';
const LOAD_SOCIAL_SUCCESS = 'redux-example/setting/LOAD_SOCIAL_SUCCESS';
const LOAD_SOCIAL_FAIL = 'redux-example/setting/LOAD_SOCIAL_FAIL';

const initialState = {
  isSetting: {},
  gettingSetting: false,
  loadedSetting: false,
  errorSetting: false,
  socials: []
};

export default function setting(state = initialState, action = {}) {
  switch (action.type) {
  case LOAD:
    return {
      ...state,
      gettingSetting: true
    };
  case LOAD_SUCCESS:
    return {
      ...state,
      isSetting: JSON.parse(action.result.des),
      loadedSetting: true
    };
  case LOAD_FAIL:
    return {
      ...state,
      errorSetting: action.err
    };

  case LOAD_SOCIAL:
    return {
      ...state,
      gettingSetting: true
    };
  case LOAD_SOCIAL_SUCCESS:
    return {
      ...state,
      socials: JSON.parse(action.result[0].des),
      loadedSetting: true
    };
  case LOAD_SOCIAL_FAIL:
    return {
      ...state,
      errorSetting: action.err
    };

  default:
    return state;
  }
}

export function isLoadedSetting(globalState) {
  return globalState.setting && globalState.setting.loadedSetting;
}

export function getSetting(lang) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(`/setting?id=${lang}`)
  };
}

export function getSettingSocials() {
  return {
    types: [LOAD_SOCIAL, LOAD_SOCIAL_SUCCESS, LOAD_SOCIAL_FAIL],
    promise: (client) => client.get(`/settingSocial`)
  };
}
